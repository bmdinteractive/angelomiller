<?php
/**
 * angelomiller functions and definitions
 */


if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0' );
}

/**
 * Enqueue Fonts and Scripts
 */
require get_stylesheet_directory() . '/inc/enqueue.php';
