// Add to a tag: aria-label="FIXME - opens in new window">
// <i class="fad fa-external-link"></i>
"use strict";



(function ($) {

	if($(".official-featherlight").length > 0){

		$(".official-featherlight").each(function(index, el) {
			var featherlightContent = $(el).attr('data-featherlight-target');
			$(el).on('click',function(evt){
				var modal = $.featherlight($(featherlightContent));
				window.modal = modal;
				modal.beforeContent = function(){
					$('.featherlight').addClass('officials');
				}
		        modal.open();
			})
	        
	    });
	}

	// Smooth Scroll
    $('a[href*="#"]')
      // Remove links that don't actually link to anything
      .not('[href="#"]')
      .not('[href="#"]')
      .not('.carousel-control-prev')
      .not('.carousel-control-next')
      .click(function(event) {
        // On-page links
        if (
          location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
          &&
          location.hostname == this.hostname
        ) {
          // Figure out element to scroll to
          let target = $(this.hash);
          target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
          // Does a scroll target exist?
          if (target.length) {
            // Only prevent default if animation is actually gonna happen
            event.preventDefault();
            let navHeight = (isMobile()) ? $('#masthead').height()* -.5 : $('#masthead').height()* -1;
            $('html, body').animate({
              scrollTop: target.offset().top+(navHeight)
            }, 750);
          }
        }
      });
})(jQuery);
