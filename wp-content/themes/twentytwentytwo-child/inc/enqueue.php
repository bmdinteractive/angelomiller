<?php 
// Remove fonts - we use only system fonts.
function child_remove_parent_function() {
	remove_action( 'wp_head', 'twentytwentytwo_preload_webfonts' );
}

add_action( 'wp_loaded', 'child_remove_parent_function' );


/**
 * Enqueue stylesheet
 */
function twentytwentytwo_child_style() {
	$theme_version  = wp_get_theme()->get( 'Version' );
	$version_string = is_string( $theme_version ) ? $theme_version : false;
	wp_register_style(
		'twentytwentytwo-child-style',
		get_stylesheet_directory_uri() . '/style.css',
		[],
		$version_string
	);
	wp_enqueue_style( 'twentytwentytwo-child-style' );

	wp_enqueue_style( 'custom_css', get_stylesheet_directory_uri() . '/css/custom.css', array(), _S_VERSION );
	wp_enqueue_style( 'custom_js', get_stylesheet_directory_uri() . '/js/custom.js', array('jquery'), _S_VERSION );
	wp_enqueue_style( 'algeria', '//fonts.cdnfonts.com/css/algeria', array(), _S_VERSION );
	wp_enqueue_script( 'fontawesome', '//kit.fontawesome.com/67042da21d.js', array(), _S_VERSION );
}

add_action( 'wp_enqueue_scripts', 'twentytwentytwo_child_style' );
add_action( 'admin_enqueue_scripts', 'twentytwentytwo_child_style' );



?>